---
label: Statistics
order: 70
icon: graph
---

# Manga Statistics

MangaDex allows users to view a Manga's rating and post their own. Additionally, some other statistics are exposed.

## Seeing a Manga's rating

```http
 GET /statistics/manga/{uuid}
```

A Manga's average rating is represented by an mean score, and
a [Bayesian](https://en.wikipedia.org/wiki/Bayesian_average) score of values between 1 and 10.

!!!info Why Bayesian?
Bayesian scores start with a set bias. This is a method to avoid little-known Manga to be greatly affected by a few
people's opinion. The Bayesian Average ultimately converges to the mean, as the reviews popoulate. Read
more [here](https://www.evanmiller.org/bayesian-average-ratings.html).
!!!

==- The Bayesian Formula

If you're interested in a little bit of math, the formula is as follows:

$$
bavg = \frac{b_c}{b_c + \frac{ab_v}{b_v}} * b_a + (1 - \frac{b_c}{b_c + \frac{ab_v}{b_v}}) * ab_a
$$

where:

- $b$ refers to manga related values
- $ab$ refers to values relating to all the manga on Mangadex
- $v$ indicates the count of votes, meaning that $b_v$ refers to the count of votes on the specific manga, and $ab_v$
  refers to the count of votes for all manga
- $c$ indicates the count of manga
- $a$ corresponds to to average of the related manga

===

##### Request

+++Python

:::code-block

```python
manga_id = "0301208d-258a-444a-8ef7-66e433d801b1"
```

:::

:::code-block

```python
import requests

base_url = "https://api.mangadex.org"

r = requests.get(f"{base_url}/statistics/manga/{manga_id}")

rating, follows, *others = r.json()["statistics"][manga_id].values()

print(
    f"Mean Rating: {rating.average}\n"
    + f"Bayesian Rating: {rating.bayesian}\n"
    + f"Follows: {follows}"
)
```

:::

+++JavaScript

:::code-block

```javascript
const mangaID = '0301208d-258a-444a-8ef7-66e433d801b1';
```

:::

:::code-block

```javascript
const axios = require('axios');

const baseUrl = 'https://api.mangadex.org';

const resp = await axios({
    method: 'GET',
    url: `${baseUrl}/statistics/manga/${mangaID}`
});

const { rating, follows } = resp.data.statistics[mangaID];

console.log(
    'Mean Rating:', rating.average, '\n' +
    'Bayesian Rating:', rating.bayesian, '\n' +
    'Follows:', follows
);

```

:::

+++
