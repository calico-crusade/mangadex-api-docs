---
order: 90
icon: dependabot
---

# reCaptcha

A small number of endpoints require a reCaptcha v3 token to proceed, in order to slow down automated malicious traffic.

Once an endpoint requires that a captcha needs to be solved, a 403 Forbidden response will be returned, with the error
code `captcha_required_exception`.
The sitekey needed for recaptcha to function is provided in both the `X-Captcha-Sitekey` header field, as well as in the
error context, specified as `siteKey` parameter.

The captcha result of the client can either be passed into the repeated original request with the `X-Captcha-Result`
header.
